<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(ChecklistSeeder::class);
        //$this->call(ProfilesTableSeeder::class);
        // $this->call(ChecklistSeeder::class);
        $this->call(NormSeeder::class);
        $this->call(SectionSeeder::class);
        $this->call(SectorSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CompanySeeder::class);
        $this->call(RelationshipSectorNormSeeder::class);
    }
}
