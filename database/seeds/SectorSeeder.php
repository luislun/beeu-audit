<?php

use Illuminate\Database\Seeder;

class SectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		DB::table('sectors')->insert([
			'name'       => 'Agricultura, pesca',
        ]);
		DB::table('sectors')->insert([
			'name'       => 'Minería e industrias extractivas',
        ]);
		DB::table('sectors')->insert([
			'name'       => 'Productos alimenticios, bebidas y tabaco',
        ]);
		DB::table('sectors')->insert([
			'name'       => 'Industria textil y productos textiles',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Cuero y productos de cuero',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Madera y productos de madera',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Pasta, papel y productos de papel',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Empresas de edición y publicación',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Artes gráficas',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Coquerías, refino de petróleo y productos',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Combustibles nucleares',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Química, productos químicos y fibras',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Productos farmacéuticos',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Productos de caucho y materias plásticas',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Productos minerales no metálicos',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Hormigón, cemento, cal, yeso, etc.',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Primera transformación de metales y productos metálicos',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Maquinaria y equipo',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Material eléctrico y óptico',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Construcción naval',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Aeronáutica y espacial',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Otros medios y equipos de transporte',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Otras industrias manufactureras no clasificadas en otros',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Reciclaje',
        ]);
        DB::table('sectors')->insert([
            'name'       => 'Producción y distribución de energía eléctrica',
        ]);     
        DB::table('sectors')->insert([
            'name'       => 'Producción y distribución de gases',
        ]); 
        DB::table('sectors')->insert([
            'name'       => 'Suministro de agua',
        ]); 
        DB::table('sectors')->insert([
            'name'       => 'Construcción',
        ]); 
        DB::table('sectors')->insert([
            'name'       => 'Comercio, reparación de vehículos de motor, motocicletas y artículos personales y de uso doméstico',
        ]); 
        DB::table('sectors')->insert([
            'name'       => 'Hoteles y restaurantes',
        ]); 
        DB::table('sectors')->insert([
            'name'       => 'Transporte, almacenamiento y comunicaciones',
        ]); 
        DB::table('sectors')->insert([
            'name'       => 'Intermediación financiera, actividades inmobiliarias, alquiler',
        ]); 
        DB::table('sectors')->insert([
            'name'       => 'Tecnología de la información',
        ]); 
        DB::table('sectors')->insert([
            'name'       => 'Servicios de ingeniería',
        ]); 
        DB::table('sectors')->insert([
            'name'       => 'Otros servicios',
        ]); 
        DB::table('sectors')->insert([
            'name'       => 'Administración pública',
        ]); 
        DB::table('sectors')->insert([
            'name'       => 'Educación',
        ]); 
        DB::table('sectors')->insert([
            'name'       => 'Actividades sanitarias. Asistencia social',
        ]); 
        DB::table('sectors')->insert([
            'name'       => 'Otras actividades sociales',
        ]);                                       

    }
}













 














