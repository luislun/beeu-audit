<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TeamAudit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE audit_teams CHANGE function function ENUM('Auditor lider', 'Auditor','Auditor tecnico', 'Traductor','Aprendiz','Testigo','Registro') default NULL;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE audit_teams CHANGE function function ENUM('Auditor lider', 'Auditor','Auditor tecnico', 'Traductor','Aprendiz','Testigo') default NULL;");
    }
}
