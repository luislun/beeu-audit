<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('status_checklists', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('exclusions')->nullable();
            $table->string('code')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
            $table->dropColumn('exclusions');
            $table->dropColumn('code');
    }
}
