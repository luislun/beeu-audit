<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificateSectorNormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificate_sector_norms', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sector_id');
            $table->unsignedBigInteger('norm_id');
            $table->unsignedBigInteger('certification_id');
            $table->foreign('sector_id')->references('id')->on('sectors');
            $table->foreign('norm_id')->references('id')->on('norms');
            $table->foreign('certification_id')->references('id')->on('certifications');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificate_sector_norms');
    }
}
