<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('result_checklists', function (Blueprint $table) {
            $table->unsignedBigInteger('result_section_id')->nullable()->after( 'description' );
            $table->foreign('result_section_id')->references('id')->on('result_section');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {        
        Schema::table('result_checklists', function (Blueprint $table) {
            $table->dropForeign(['result_section_id']);
            $table->dropColumn('result_section_id');
        });
    }
}
