<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Profiles;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','lastname','secondname', 'email', 'password','avatar','profile','cellphone','code','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profiles()
    {
        return $this->hasOne('App\Profiles','id','profile');
    }

    public function AuditorSector()
    {
        return $this->hasMany('App\AuditorSector','user_id','id');
    }

    public function AuditorHistory()
    {
        return $this->hasMany('App\AuditHistory','user_id','id');
    }

    public function JobHistory()
    {
        return $this->hasMany('App\JobHistory','user_id','id');
    }

    public function YearceHistory()
    {
        return $this->hasMany('App\YearceHistory','user_id','id');
    }

    public function ExperienceHistory()
    {
        return $this->hasMany('App\ExperienceHistory','user_id','id');
    }

    public function EvaluationHistory()
    {
        return $this->hasMany('App\EvaluationHistory','user_id','id');
    }





}
