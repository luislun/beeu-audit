<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conform extends Model
{
	protected $table    = 'conforms';
	
	protected $fillable = [ 'code','description','type_nc','statuscheck_id'];
	
	protected $guarded  = [ 'id' ];

}
