<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditTeam extends Model
{
    public $timestamps  = true;

	protected $table    = 'audit_teams';

	protected $fillable = ['user_id','audit_id','function','date','hour','minute','date_end'];   

	protected $guarded  = ['id'];

	public function user()
	{
  		return $this->belongsTo('App\User');

	}

}
