<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certification extends Model
{

	protected $table    = 'certifications';
	
	protected $fillable = [ 'init_date','end_date','status','organization_id','no_certificate'];
	
	protected $guarded  = [ 'id' ];
   
   	public function OrganizationSector()
	{
		return $this->belongsTo('App\CertificateSectorNorm','id','certification_id');
	}

	public function certification()
	{
		return $this->belongsTo('App\Organization','organization_id','id');
	}
	public function sectorsandnorms()
	{
		return $this->hasMany('App\CertificateSectorNorm');
	}
	public function audit()
	{
		return $this->hasMany('App\Audit');
	}


}
