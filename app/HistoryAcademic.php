<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryAcademic extends Model
{
    protected $table    = 'history_academics';
	
	protected $fillable = ['date','pass','score','file_path','user_id','institution','carrier','certification_institution'];
}
