<div class="modal" tabindex="-1" id="new" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        
      <div class="modal-header">
        <h5 class="modal-title">Agregar curso</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

	    <form class="form-horizontal" method="post" action="{{ route('savecourse') }}" accept-charset="UTF-8" enctype="multipart/form-data">
			{{csrf_field()}}
	      <div class="modal-body">

            <input type="hidden" name="user_id" id="user_id" value="{{$u->id}}">

              <div class="form-group">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">INSTITUCIÓN</label>
                  <input type="text" class="form-control" placeholder="INSTITUCIÓN" name="namecourse">
              </div>

              <div class="form-group">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">PERIODO</label>
                  <input type="text" class="form-control" placeholder="PERIODO" name="datecourse">
              </div>

              <div class="form-group">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CARRERA/POSGRADO</label>
                  <input type="text" class="form-control" name="passcourse" placeholder="CARRERA/POSGRADO">
              </div>

              <div class="form-group">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CALIFICACIÓN</label>
                  <input type="text" class="form-control" name="scorecourse" placeholder="CALIFICACIÓN">
              </div>

              <div class="form-group">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">TITULO,DIPLOMA.CERTIFICACO,ETC</label>
                  <input type="text" class="form-control" name="titlecertificate" placeholder="TITULO,DIPLOMA.CERTIFICACO,ETC">
              </div>

            <div class="custom-file">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">Archivos </label>
				<input type="file" class="custom-file-input" id="customFileLang" lang="es" name="filecourse">
				<label class="custom-file-label" for="customFileLang">Seleccionar Archivo</label>
			</div>

	      </div>

	      <div class="modal-footer">
	            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
	            <button type="submit" class="btn btn-primary"> <i class="fa fa-paper-plane" aria-hidden="true"></i>Guardar</button>
	      </div>

	  </form>
    </div>
  </div>
</div>
