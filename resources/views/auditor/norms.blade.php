<div class="modal" tabindex="-1" id="norms" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar curso</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	    <form class="form-horizontal" method="post" action="{{ route('savecourse') }}" accept-charset="UTF-8" enctype="multipart/form-data">
			{{csrf_field()}}
	      <div class="modal-body">
            <div class="tx-13 mg-b-25 ">
              
                <div id="wizard1">
                  <h3>Sectores</h3>
                  <section>
                      <div class="form-group">
                        <select class="custom-select selectsectors" required onchange="getvalsector(this);" id="selectsect">
                          <option  disabled selected="">Selecciona un sector</option>
                          @foreach($sectores as $sector)
                            <option value="{{$sector->id}}">{{$sector->name}}</option>
                          @endforeach
                        </select>
                      </div>
                  </section>
                  <h3>Normas</h3>
                  <section>
                      <div class="form-group">
                        <select class="custom-select" required onchange="getvalnorm(this);" id="selectnrom">
                          <option value="" disabled selected="">Selecciona una norma</option>
                          @foreach($norms as $norma)
                            <option value="{{$norma->id}}">{{$norma->name}}</option>
                          @endforeach
                        </select>
                      </div>
                  </section>
                </div>

              </div><!-- df-example -->
	      </div>

	      <div class="modal-footer">
	            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
	            <button type="submit" class="btn btn-primary"> <i class="fa fa-paper-plane" aria-hidden="true"></i>Guardar</button>
	      </div>

	  </form>
    </div>
  </div>
</div>
