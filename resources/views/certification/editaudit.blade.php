<div class="modal" tabindex="-1" id="editAudit" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title">Editar auditoria</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

	   	<form action="{{route('editcertification')}}" method="post">
				{{csrf_field()}}
      		<div class="modal-body pd-sm-t-30 pd-sm-b-40 pd-sm-x-30">
        	  <div class="form-group">
                <label >Fecha de inicio</label>
                <input type="date" name="init_date" class="form-control" required="required" id="date">
            </div>
	          <input type="hidden" name="id" id="id" value="">
          </div>

          <div class="modal-footer pd-x-20 pd-y-15">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fas fa-undo"></i> Cancelar</button>
            <button type="submit" class="btn btn-primary"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Guardar</button>
          </div>
          
	    </form>
    </div>
  </div>
</div>
