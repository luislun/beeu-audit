@extends('layouts.app')
    @section('css')
        <style type="text/css">

        </style>
    @endsection

@section('content')
       <nav aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-style1 mg-b-0">
            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Auditores</li>
          </ol>
          <br>
        </nav>

        <div class="col-sm-12 col-lg-12 col-md-12">
          <div class="card card-body">
            <div class="container-fluid">

              <div class="row">
                <div class="col-md-6">
                    <h6 class="tx-uppercase tx-20 tx-spacing-1 tx-color-02 tx-semibold mg-b-8 col-lg-10"><i data-feather="folder-plus"></i> Certificacion</h6>
                </div>
                <div class="col-md-6">
                    @if(in_array(Auth::user()->profile,['Administrador','Comercial']))
                      <a href="#new"  class="btn  btn-dark tx-spacing-1 tx-semibold  col-lg-4 col-md-4 col-sm-12 text-white newbutton" style="float: right;" data-toggle="modal" data-animation="effect-sign" > <i class="fas fa-plus"></i> Nuevo</a>
                    @endif
                </div>
              </div>

              <hr>

            </div>

            <form>

            <div class="container-fluid">
              <div class="row">
                <div class="col-lg-4 col-sm-12 col-md-12">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">SECTOR</label>
                    <select class="custom-select" name="sectors">
                        <option selected disabled>Seleccionar...</option> 
                        @foreach($sectors as $sector)
                                <option value="{{$sector->id}}">{{$sector->id}} --- {{$sector->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-lg-4 col-sm-12 col-md-12">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NORMA</label>
                    <select class="custom-select" name="norms">
                        <option selected disabled>Seleccionar...</option>
                            @foreach($norms as $norm)
                                    <option value="{{$norm->id}}">{{$norm->name}}</option>
                            @endforeach
                    </select>
                </div>

                <div class="col-lg-4 col-sm-12 col-md-12">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NOMBRE,RFC o CÓDIGO</label>
                    <input name="search" class="form-control" type="search" placeholder="Buscar por nombre,rfc o código" aria-label="Search">
                </div>

              </div>
            </div>
            <div class="container-fluid">
              <div class="row">
                  <div class="col-lg-12 col-sm-12 col-md-12">
                      <br>
                      <button class="btn bg-dark text-white" type="submit"><i class="fa fa-search"></i> Buscar</button>
                      <button class="btn bg-dark text-white" type="submit"> Ver todos</button>
                  </div>
              </div>
            </div>
            
            <hr>

            </form>

            <div class="container-fluid ">
              <div class="table-responsive ">
                <table class="table table-striped table-sm  table-bordered ">
                  <thead>
                    <tr>
                      <th>Código</th>
                      <th>Organizacion</th>
                      <th class="d-none d-md-table-cell">RFC</th>
                      <th class="d-none d-md-table-cell">Fecha de inicio</th>
                      <th class="d-none d-md-table-cell">Fecha termino</th>
                      <th class="d-none d-md-table-cell">Sectores</th>
                      <th class="d-none d-md-table-cell">Normas</th>
                      <th class="d-none d-md-table-cell">Contacto</th>
                      <th class="d-none d-md-table-cell">Status</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                      @if(in_array(Auth::user()->profile, ['Administrador', 'Comercial','Planeador']))
                        @foreach($organizations as $organization)
                        @foreach($organization->certification as $organizationdetail)
                          @if($organization->certification)
                          <tr>
                            <td>{{$organization->code}}</td>
                            <td>{{$organization->name}}</td>
                            <td>{{$organization->rfc}}</td>
                            <td>{{$organizationdetail->init_date}}</td>
                            <td>{{$organizationdetail->end_date}}</td>
                            <td>
                              @foreach($organizationdetail->sectorsandnorms as $sectornorm)
                                <li class="{{$organizationdetail->id}}{{$sectornorm->sector->id}}Sector">{{$sectornorm->sector->name}}</li>
                              @endforeach
                            </td>
                            <td>
                              @foreach($organizationdetail->sectorsandnorms as $sectornorm)
                                <li class="{{$organizationdetail->id}}{{$sectornorm->norm->id}}Norma">{{$sectornorm->norm->name}} </li>
                              @endforeach
                            </td>
                            <td>{{$organization->directionCertification->phone}}</td>
                            <td>@if($organizationdetail->status==='Activo')
                                <span class="badge badge-warning">Activa</span>
                                @elseif($organizationdetail->status==='Finalizado')
                                <span class="badge badge-success">Finalizado</span>
                                @else
                                <span class="badge badge-danger">Cancelado</span>
                                @endif
                            </td>
                            <td>
                              @if(in_array(Auth::user()->profile,['Administrador']))
                                @if($organizationdetail->status==='Activo')
                                  <a data-toggle="modal" data-animation="effect-sign" href="#changeStatus{{$organizationdetail->id}}" class="btn btn-light btn-icon" ><i class="fas fa-minus-circle" style="color:#BB0404" ></i> </a>
                                @elseif($organizationdetail->status==='Cancelado')
                                  <a data-toggle="modal" data-animation="effect-sign" href="#changeStatus{{$organizationdetail->id}}" class="btn btn-light btn-icon" ><i class="fas fa-check" style="color:#00C0FE"></i> </a>
                                @endif
                              @endif

                              @if(count($organizationdetail->audit) >= 1)
                                @if(in_array(Auth::user()->profile,['Administrador', 'Planeador','Comercial','Auditor','Registro','Tecnico']))
                                    <a href="progress/{{$organizationdetail->id}}" class="btn btn-light btn-icon" ><i class="fas fa-eye"></i></a>
                                @endif

                              @else
                                    
                              @endif
                              @if(count($organizationdetail->audit) >= 1)
                              
                              @else
                                @if($organizationdetail->status!='Cancelado')
                                      @if(in_array(Auth::user()->profile,['Administrador','Planeador']))
                                        <a href="#start{{$organizationdetail->id}}" class="btn btn-light btn-icon"  data-toggle="modal" data-animation="effect-sign"><i class="fab fa-font-awesome-flag"style="color:#00C0FE" ></i></a>
                                      @endif
                                      @if(in_array(Auth::user()->profile,['Administrador','Comercial']))
                                        <button class="btn btn-light danger-btn btn-icon" data-toggle="modal" href="#delete" data-animation="effect-sign" data-id="{{$organizationdetail->id}}"><i data-feather="trash-2" style="color: red"></i></button>
                                         <button class="btn btn-light danger-btn btn-icon" data-toggle="modal" href="#editAudit" data-animation="effect-sign" data-id="{{$organizationdetail->id}}" data-date="{{$organizationdetail->init_date}}" ><i data-feather="edit" style="color:#48bb78" ></i></button>
                                      @endif
                                @endif
                              @endif
                            </td>
                          </tr>
                          @endif
                        @endforeach
                      @endforeach
                    @else
						{{-- AUDITORES --}}




						@foreach($organizations as $organization)
							@foreach($organization->certification as $organizationdetail)
						  		@if($certificationUtils->validTeam($organizationdetail,Auth::user()->id))
									<tr class="{{$organizationdetail->no_certificate}}">
										<td>{{$organization->code}}</td>
										<td>{{$organization->name}}</td>
										<td>{{$organization->rfc}}</td>
										<td>{{$organizationdetail->init_date}}</td>
										<td>{{$organizationdetail->end_date}}</td>
										<td>
											@foreach($organizationdetail->sectorsandnorms as $sectornorm)
											<li class="{{$organizationdetail->id}}{{$sectornorm->sector->id}}Sector">{{$sectornorm->sector->name}}</li>
											@endforeach
										</td>
										<td>
											@foreach($organizationdetail->sectorsandnorms as $sectornorm)
											<li class="{{$organizationdetail->id}}{{$sectornorm->norm->id}}Norma">{{$sectornorm->norm->name}} </li>
											@endforeach
										</td>
										<td>{{$organization->directionCertification->phone}}</td>
										<td>
											@if($organizationdetail->status==='Activo')
											<span class="badge badge-warning">Activa</span>
											@elseif($organizationdetail->status==='Finalizado')
											<span class="badge badge-success">Finalizado</span>
											@else
											<span class="badge badge-danger">Cancelado</span>
											@endif
										</td>
										<td>
											@if(count($organizationdetail->audit) >= 1)
											@if(in_array(Auth::user()->profile,['Auditor','Registro','Tecnico']))
											<a href="progress/{{$organizationdetail->id}}" class="btn btn-light btn-icon" ><i class="fas fa-eye"></i></a>
											@endif
											@endif
										</td>
									</tr>
								@endif
							@endforeach
						@endforeach
					@endif
				</tbody>
			</table>
			</div>
			@if (count($organizations))
				<div name="paginador" style="float: left;">
					{{ $organizations->links() }}
				</div>
			@endif
		</div>
	</div>
</div>

@extends('certification.startcertificarion')
@extends('certification.addcertification')
@extends('certification.delete')
@extends('certification.editaudit')
@extends('certification.changeStatus')

@endsection

@section('scripts')
        
      @foreach($organizations as $organization)
        @foreach($organization->certification as $organizationdetail)
          @foreach($organizationdetail->sectorsandnorms as $sectornorm)
          <script>
            $(document).ready(function() {
                $('.{{$organizationdetail->id}}{{$sectornorm->sector->id}}Sector').addClass('d-none');
                $('.{{$organizationdetail->id}}{{$sectornorm->sector->id}}Sector').first().removeClass('d-none');
                $('.{{$organizationdetail->id}}{{$sectornorm->norm->id}}Norma').addClass('d-none');
                $('.{{$organizationdetail->id}}{{$sectornorm->norm->id}}Norma').first().removeClass('d-none');
            });
          </script>
          @endforeach
        @endforeach
      @endforeach


      @foreach($organizations as $organization)
        @if($organization->certification)
          @foreach($organization->certification as $organizationdetail)
            @foreach($organizationdetail->sectorsandnorms as $sectorsandnorms)
              <script>
                $(document).ready(function() {
                    $('.{{$organizationdetail->id}}{{$sectorsandnorms->sector->id}}SectorAgregarNombre').addClass('d-none');
                    $('.{{$organizationdetail->id}}{{$sectorsandnorms->sector->id}}SectorAgregarNombre').first().removeClass('d-none');
                    $('.{{$organizationdetail->id}}{{$sectorsandnorms->norm->id}}NormaAgregarNombre').addClass('d-none');
                    $('.{{$organizationdetail->id}}{{$sectorsandnorms->norm->id}}NormaAgregarNombre').first().removeClass('d-none');
                });
              </script>
            @endforeach
          @endforeach
        @endif
      @endforeach





<script type="text/javascript">
      $('#delete').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var modal  = $(this)
        modal.find('#id').val(id);
      })

      $('#editAudit').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id   = button.data('id')
        var date = button.data('date')
        var modal  = $(this)
        modal.find('#id').val(id);
        modal.find('#date').val(date);
      })

      jQuery(document).ready(function($) {
        $('.select2').select2({
          dropdownParent: $('#new'),
          maximumSelectionLength: 1,
            language: {
                noResults: function() {
                  return "No hay resultados";        
                },
                searching: function() {
                  return "Buscando..";
                },
                maximumSelected:function(){
                  return "Solo se puede elegir un registro";
                }
              } 

        });
        $('#start').on('show.bs.modal', function (event) {
          var button       = $(event.relatedTarget) 
          var organization = button.data('organization') 
          var id           = button.data('id') 
          var sectores     = button.data('sectores')
          var modal        = $(this)
          modal.find('.modal-body #id').val(id);
          modal.find('.modal-body #organization').val(organization);
          $('#sectores').text(sectores)
        })
      });

      $("#organizationId").on("change",function(e){
        var id = $(this).val();

        var $normsandsectors = $('.normsandsectors');
        var $normsandsectors2 = $('.normsandsectors2');


          $.ajax({
            type:'POST',
            url:'{{route('changesection')}}',
            data:{id:id,_token: "{{ csrf_token() }}"},
            success:function(data){
              var clasesSector = [];
              var clasesNorm   = [];

                $normsandsectors.empty();
               $.each(data, function(value, key ) {
                   $normsandsectors.append('<input type="hidden" name="detailsorganization[]" value="'+key.id+'"><li class="' + key.sector_id + 'Sector">'+key.name+'</li>'); 
                   $normsandsectors2.append('<li class="'+key.norm_id+'Norms">'+key.norma+'</li>'); 

                   clasesSector[key.sector_id] = "."+ key.sector_id + 'Sector';
                   clasesNorm[key.norm_id] = "."+ key.norm_id + 'Norms';
               });

               $.each(clasesSector, function(value, key ) {
                  $(key).addClass('d-none');
                  $(key).first().removeClass('d-none');
               });

              $.each(clasesNorm, function(value, key ) {
                  $(key).addClass('d-none');
                  $(key).first().removeClass('d-none');
               });

               $('#normsandsectors').attr("style", "pointer-events: none;");

            },
            error:function(data){
            }
        });
      });



      $( ".fechas" ).change(function() {
        var fechainicial = $(this).closest('.validation').find($('.date_start')).val();
        var fechafinal = $(this).closest('.validation').find($('.date_finish')).val();
        
        if(fechafinal < fechainicial){
          $('.removerValidation').remove()
          $('.btnValidation').prop('disabled', true);
          $('.validation').after("<a class='text-danger removerValidation'>La fecha final debe ser mayor a la fecha inicial<a/>");
        }else{
          $('.btnValidation').prop('disabled', false);
          $('.removerValidation').remove()

        }
      });

</script>

@endsection 


