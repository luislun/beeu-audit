@extends('layouts.app')
  @section('css')
      <style type="text/css">
          .avatar{
              width: 60px;
              height:60px;
              border-radius: 13px;
          }
          .imagen img{
              width: 50px !important;
              border-radius: 10px;
            }

      </style>
  @endsection

@section('content')

@foreach($user as $u)
       <nav aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-style1 mg-b-0">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{ URL::previous() }}">Usuarios</a></li>
            <li class="breadcrumb-item active" aria-current="page">Editar Usuario {{$u->name}} </li>
          </ol>
          <br>
        </nav>

      <div class="modal-content">
        <form class="form-horizontal" method="post" action="{{ route('user.update',$u->id) }}" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="modal-header pd-y-20 pd-x-20 pd-sm-x-30">
            <div class="media align-items-center">
                <div id="preview" class="imagen text-center">
                  <img src="{{ asset("storage/$u->avatar")}}"> 
                </div>
                  
              <div class="media-body mg-sm-l-20">
                <h3 class="tx-18 tx-sm-20 mg-b-2 tx-uppercase">DATOS DEL USUARIO {{$u->name}}</h3>
              </div>
            </div>
          </div>
          <div class="modal-body pd-sm-t-30 pd-sm-b-40 pd-sm-x-30">

            <div class="form-group">
              <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">FOTO DE PERFIL</label>
                <div class="custom-file">
                  <input type="file" name="img" size="150"  maxlength="150" accept="image/jpeg,image/png" class="form-group custom-file-input" id="file" capture="camera"/>
                  <label class="custom-file-label" for="customFile">.15789_{{$u->name}}.jpg</label>
                  <input type="hidden" value="{{$u->avatar}}" name="avatar">
              </div>
            </div>

            <div class="row row-sm">
                <div class="col-sm">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NOMBRE</label>
                  <input type="text" class="form-control" placeholder="NOMBRE" value="{{$u->name}}" name="name" required="required">
                </div>
                <div class="col-sm mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">APELLIDO PATERNO</label>
                  <input type="text" class="form-control" placeholder="APELLIDO PATERNO" value="{{$u->lastname}}" name="lastname" required="required">
                </div>
                <div class="col-sm mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">APELLIDO MATERNO</label>
                  <input type="text" class="form-control" placeholder="APELLIDO MATERNO" value="{{$u->secondname}}" name="secondname" required="required">
                </div>
            </div>

            <div class="row row-sm mt-3">
                <div class="col-sm">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CORREO</label>
                  <input type="text" class="form-control" placeholder="CORREO" value="{{$u->email}}" name="email" required="required">
                </div>
                <div class="col-sm mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CONTRASEÑA</label>
                  <input type="password" class="form-control" placeholder="********" name="password">
                </div>
            </div>

            <div class="row row-sm mt-3">
                <div class="col-sm">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">TELEFONO</label>
                  <input type="text" class="form-control" placeholder="TELEFONO" value="{{$u->cellphone}}" name="cellphone" required="required">
                </div>

                <div class="col-sm mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CODIGO</label>
                  <input type="text" class="form-control" placeholder="CODIGO" value="{{$u->code}}" name="code" required="required"> 
                </div>

                <div class="col-sm mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">PERFIL</label>
                  <select class="form-control custom-select" value="{{$u->profile}}" name="profile" required="required">
                    @if($u->profile=='Comercial')
                      <option value="Comercial" selected>Comercial</option>
                      <option value="Planeador">Planeador</option>
                      <!-- <option value="Registro">Registro</option> -->
                      <!-- <option value="Tecnico">Técnico</option> -->
                      <option value="Administrador">Administrador</option>
                    @endif
                    @if($u->profile=='Planeador')
                      <option value="Planeador" selected>Planeador</option>
                      <option value="Comercial">Comercial</option>
                      <!-- <option value="Registro">Registro</option> -->
                      <!-- <option value="Tecnico">Técnico</option> -->
                      <option value="Administrador">Administrador</option>
                    @endif
                    <!-- @if($u->profile=='Registro')
                      <option value="Registro" selected>Registro</option>
                      <option value="Planeador">Planeador</option>
                      <option value="Comercial">Comercial</option>
                      <option value="Tecnico">Técnico</option>
                      <option value="Administrador">Administrador</option>
                    @endif -->
                    <!-- @if($u->profile=='Tecnico')
                      <option value="Tecnico" selected>Técnico</option>
                      <option value="Planeador">Planeador</option>
                      <option value="Comercial">Comercial</option>
                      <option value="Registro">Registro</option>
                      <option value="Administrador">Administrador</option>
                    @endif -->
                    @if($u->profile=='Administrador')
                      <option value="Administrador" selected>Administrador</option>
                      <option value="Planeador">Planeador</option>
                      <option value="Comercial">Comercial</option>
                      <!-- <option value="Registro">Registro</option> -->
                      <!-- <option value="Tecnico">Técnico</option> -->
                    @endif

                  </select>
                </div>

                <div class="col-sm mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">ESTADO</label>
                  <select class="form-control custom-select" value="{{$u->status}}" name="status" required="required">
                        <option disabled >Escoge una opción</option>
                         @if($u->status=="Activo")
                            <option value="Activo" selected>Activo</option>
                            <option value="Inactivo">Inactivo</option>
                            <option value="Suspendido">Suspendido</option>
                          @elseif($u->status=="Inactivo")
                            <option value="Inactivo" selected>Inactivo</option>
                            <option value="Activo" >Activo</option>
                            <option value="Suspendido">Suspendido</option>
                          @else
                            <option value="Suspendido" selected>Suspendido</option>
                            <option value="Activo">Activo</option>
                            <option value="Inactivo">Inactivo</option>
                          @endif
                  </select>
                </div>

            </div>

          </div>

          <div class="modal-footer pd-x-20 pd-y-15">
            <a type="button" class="btn btn-white" href="{{route('users')}}" ><i class="fas fa-undo"></i> Cancelar</a>
            <button type="submit" class="btn btn-primary"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Guardar</button>
          </div>
          
        </form>
    </div>
@endforeach
  @section('scripts')
    <script>
      document.getElementById("file").onchange = function (e) {
        let reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);
            reader.onload     = function () {
            let preview       = document.getElementById("preview"),
            image             = document.createElement("img");  
            image.src         = reader.result;
            preview.innerHTML = "";
            preview.append(image);
        };
      };
    </script>
  @endsection 
@endsection


